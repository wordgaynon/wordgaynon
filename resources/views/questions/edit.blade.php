@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Details<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route( 'questions.update', $question->id ) }}" method="POST" enctype="multipart/form-data" role="form">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT">
				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="username">Name</label>
							<input type="text" value="{{ $question->name }}" name="name" class="form-control" id="username" placeholder="Name">
						</div>

						
						<div class="form-group">
							<label for="image">Image <small>(<i>300x200 pixels</i>)</small></label>
							<input type="file" value="" name="image" class="form-control" id="image" placeholder="Image">
							@if( $question->image ) 
							<br>
							<a href="#" data-img="{{ url( '/uploads/' . $question->teacher_id . '/' . strtolower( $question->category->name ) . '/' . strtolower( $question->level->name ) . '/' . $question->image  ) }}" 
								class="btn btn-xs btn-info" data-filename="{{ $question->image }}"
								data-toggle="modal" data-target="#image-modal">View image <i class="fa fa-picture-o"></i></a>
							@endif
						</div>
					
					
						<div class="form-group">
							<label for="sound">Sound</label>
							<input type="file" value="" name="sound" class="form-control" id="sound" placeholder="Sound">
							@if( $question->sound )
							<br>
							<a href="#" data-mp3="{{ url( '/uploads/' . $question->teacher_id . '/' . strtolower( $question->category->name ) . '/' . strtolower( $question->level->name ) . '/' . $question->sound  ) }}" 
								class="btn btn-xs btn-info" data-filename="{{ $question->sound }}"
								data-toggle="modal" data-target="#sound-modal">Listen <i class="fa fa-volume-up"></i></a>
							@endif
						</div>

						<div class="form-group">
							<label for="category">Category</label>
							<select name="category" class="form-control" id="category">
								<option value="">---- Select Category</option>
								@foreach( $categories as $cat )
								<option value="{{ $cat->id }}"{{ $question->category_id == $cat->id ? ' selected':''}}>{{ ucfirst( $cat->name ) }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="level">Level</label>
							<select name="level" class="form-control" id="level"></select>
						</div>

						<button type="submit" class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>


<div class="modal inmodal" id="image-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" align="center">
            	<img width="100%" src="#" alt="" class="img-responsive">
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="sound-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" align="center">
				
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
{{-- <a href="{{ route('roles.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a> --}}
</div>
@endsection

@section('styles')
@endsection

@section('scripts')
<script>
jQuery(document).ready(function() {

	jQuery('#image-modal').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
  		jQuery(this).find('.modal-title').text( button.data('filename') );
  		var img = button.data('img');
  		jQuery(this).find('.img-responsive').attr('src', img);
	});

	jQuery('#sound-modal').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
  		jQuery(this).find('.modal-title').text( button.data('filename') );
  		var mp3 = '<audio autoplay="true" controls><source src="'+button.data('mp3')+'" type="audio/mpeg">Your browser does not support the audio element.</audio>';
  		jQuery(this).find('.modal-body').html(mp3);
	});

	var levels = [@foreach( $levels as $level ){cat:{{ $level->category->id }},id:{{$level->id}},name:'{{ ucwords( str_replace("_", " ", $level->name ) ) }}'},@endforeach];
	var level = {{ $question->level_id }};
	var cat_id = {{ $question->category_id }};

	for (var i = levels.length - 1; i >= 0; i--) {
		if ( levels[i].cat == cat_id ) {
			var selected = ( level==levels[i].id ) ? ' selected':''; 
			jQuery('#level').append('<option value="'+ levels[i].id +'"'+selected+'>'+levels[i].name+'</option>');
		}
	}

	jQuery('#category').change(function(event) {
		var cat_id = jQuery(this).val();
		jQuery('#level').html('');
		for (var i = levels.length - 1; i >= 0; i--) {
			if ( levels[i].cat == cat_id ) {
				jQuery('#level').append('<option value="'+ levels[i].id +'">'+levels[i].name+'</option>');
			}
		}
	});
});
</script>
@endsection

