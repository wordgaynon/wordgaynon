@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Questions<small class="m-l-sm">Manage all questions.</small></h5>
		</div>
		<div class="ibox-content">
			
			<div class="row">
				<div class="col-md-6 col-sm-6">
					
					<form action="{{ route('questions.index') }}" method="GET" class="form-inline" role="form">

						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">per page</span> 
								<input type="number" name="perpage" value="{{ $perpage }}" placeholder="10" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<label class="sr-only" for="">Category</label>
							<select class="form-control" name="category" id="select-category">
								<option value="">Category ----</option>
								@if( $categories->count() )
								@foreach( $categories as $cat )
								<option value="{{ $cat->id }}"{{ ( $category_id == $cat->id ) ? ' selected':'' }}>{{ ucfirst( $cat->name ) }}</option>
								@endforeach
								@endif
							</select>
						</div>

						<div class="form-group">
							<label class="sr-only" for="">Level</label>
							<select class="form-control" name="level" id="select-level">
								<option value="">Level ----</option>
							</select>
						</div>

						<button type="submit" style="margin: 0;" class="btn btn-info">Filter <i class="fa fa-filter"></i></button>
					</form>

				</div>	
				<div class="col-md-6 col-sm-6">
					<form action="{{ route('questions.index') }}" method="GET" class="form-inline pull-right" role="form">
						<div class="input-group">
							<input type="text" name="search" value="{{ $search }}" placeholder="Keyword" class="form-control">
							<span class="input-group-btn"> 
								<button type="submit" class="btn btn-info">Search <i class="fa fa-search"></i></button>
							</span>
						</div>
					</form>
				</div>	
			</div>

			<hr>
			

			@if( $questions->count() )
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Category</th>
							<th>Level</th>
							<th>Image</th>
							<th>Audio</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $questions as $question )

							<tr id="user-row-{{ $question->id }}">
								<td>{{ ucfirst( $question->name ) }}</td>
								<td>{{ ucwords( str_replace("_", " ", $question->category->name ) ) }}</td>
								<td>{{ ucwords( str_replace("_", " ", $question->level->name ) ) }}</td>
								<td>
									@if( $question->image && file_exists( public_path() . '/uploads/' . $question->teacher_id . '/' . strtolower( $question->category->name ) . '/' . strtolower( $question->level->name ) . '/' . $question->image ) )
									<button data-img="{{ url( '/uploads/' . $question->teacher_id . '/' . strtolower( $question->category->name ) . '/' . strtolower( $question->level->name ) . '/' . $question->image  ) }}" 
										class="btn btn-xs btn-info" data-filename="{{ $question->image }}"
										data-toggle="modal" data-target="#image-modal">View image <i class="fa fa-picture-o"></i></button>
									@else
									<span>No Image</span>
									@endif
								</td>
								<td>
									@if( $question->sound && file_exists( public_path() . '/uploads/' . $question->teacher_id . '/' . strtolower( $question->category->name ) . '/' . strtolower( $question->level->name ) . '/' . $question->sound ) )
									<button data-mp3="{{ url( '/uploads/' . $question->teacher_id . '/' . strtolower( $question->category->name ) . '/' . strtolower( $question->level->name ) . '/' . $question->sound  ) }}" 
										class="btn btn-xs btn-info" data-filename="{{ $question->sound }}"
										data-toggle="modal" data-target="#sound-modal">Listen <i class="fa fa-volume-up"></i></button>
									@else
									<span>No Audio</span>
									@endif
								</td>
								<td>
									<a href="{{ route('questions.edit', $question->id) }}" class="btn btn-success btn-xs">
										<i class="fa fa-edit"></i> Edit
									</a>
									<button data-id="{{ $question->id }}" data-cat="{{ $question->name }}" data-name="delete" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#remove-user">
										<i class="fa fa-close"></i> Delete
									</button>
								</td>
							</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			
			{{ 
				$questions->appends([
					'category' => $category_id, 
					'perpage' => $perpage,
					'level' => $level_id
				])->links() 
			}}
			@else 

			<div class="alert alert-warning">
				<strong>No records!</strong> you may now start creating new questions.
			</div>

			@endif

		</div>
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove Question</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, this question will no longer be available to all users.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Cancel</button>
                <button type="button" id="remove-btn" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="image-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" align="center">
            	<img width="100%" src="#" alt="" class="img-responsive">
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="sound-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" align="center">
				
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route('questions.create') }}" class="btn btn-primary">Add question <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	var levels = [@foreach( $levels as $level ){cat:{{ $level->category->id }},id:{{$level->id}},name:'{{ ucwords( str_replace("_", " ", $level->name ) ) }}'},@endforeach];
	var level_id = '{{ $level_id }}';
	var cat_id = '{{ $category_id }}';

	jQuery('#select-level').html('<option value="">Level ----</option>');
	for (var x = levels.length - 1; x >= 0; x--) {
		if ( levels[x].cat == cat_id ) {
			var selected = ( level_id==levels[x].id ) ? ' selected':''; 
			jQuery('#select-level').append('<option value="'+ levels[x].id +'"'+selected+'>'+levels[x].name+'</option>');
		}
	}

	jQuery('#select-category').change(function(event) {
		var cats_id = jQuery(this).val();
		jQuery('#select-level').html('<option value="">Level ----</option>');
		for (var i = levels.length - 1; i >= 0; i--) {
			if ( levels[i].cat == cats_id ) {
				jQuery('#select-level').append('<option value="'+ levels[i].id +'">'+levels[i].name+'</option>');
			}
		}
	});

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#image-modal').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
  		jQuery(this).find('.modal-title').text( button.data('filename') );
  		var img = button.data('img');
  		jQuery(this).find('.img-responsive').attr('src', img);
	});

	jQuery('#sound-modal').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
  		jQuery(this).find('.modal-title').text( button.data('filename') );
  		var mp3 = '<audio autoplay="true" controls><source src="'+button.data('mp3')+'" type="audio/mpeg">Your browser does not support the audio element.</audio>';
  		jQuery(this).find('.modal-body').html(mp3);
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var question_id = button.data('id');
		jQuery('#remove-btn').data('id',question_id);
  		jQuery(this).find('.modal-title').text('Remove ' + button.data('cat') );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var question_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('questions.index') }}/'+question_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if ( data.error ) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+question_id).remove();
			}
		});
		 
	});

});
</script>
@endsection
