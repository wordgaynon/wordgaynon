@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Add Question<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route('questions.store') }}" method="POST" enctype="multipart/form-data" role="form">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="name">Name</label>
							<input type="text" maxlength="10" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="Name">
						</div>

						<div class="form-group">
							<label for="image">Image <small>(<i>300x200 pixels</i>)</small></label>
							<input type="file" value="{{ old('image') }}" name="image" class="form-control" id="image" placeholder="Image">
						</div>

						<div class="form-group">
							<label for="sound">Sound</label>
							<input type="file" value="{{ old('sound') }}" name="sound" class="form-control" id="sound" placeholder="Sound">
						</div>

						<div class="form-group">
							<label for="category">Category</label>
							<select name="category" class="form-control" id="category">
								<option value="">---- Select Category</option>
								@foreach( $categories as $cat )
								<option value="{{ $cat->id }}"{{ old('category') == $cat->id ? ' selected':''}}>{{ ucfirst( $cat->name ) }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="level">Level</label>
							<select name="level" class="form-control" id="level"></select>
						</div>

						<button type="submit" class="btn btn-primary">Save Question <i class="fa fa-save"></i></button>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('questions.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection

@section('styles')
@endsection

@section('scripts')
<script>
jQuery(document).ready(function() {
	var levels = [@foreach( $levels as $level ){cat:{{ $level->category->id }},id:{{$level->id}},name:'{{ ucwords( str_replace("_", " ", $level->name ) ) }}'},@endforeach];
	jQuery('#category').change(function(event) {
		var cat_id = jQuery(this).val();
		jQuery('#level').html('');
		for (var i = levels.length - 1; i >= 0; i--) {
			if ( levels[i].cat == cat_id ) {
				jQuery('#level').append('<option value="'+ levels[i].id +'">'+levels[i].name+'</option>');
			}
		}
	});
});
</script>
@endsection