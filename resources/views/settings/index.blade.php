@extends('layouts.admin')

@section('content')
<div class="col-lg-6">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>User Information<small class="m-l-sm">Edit your account information.</small></h5>
		</div>
		<div class="ibox-content">
			<form action="{{ route('profile.update') }}" method="POST" role="form">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="username">Username</label>
					<input type="text" value="{{ Auth::user()->name }}" name="name" class="form-control" id="username" placeholder="username">
				</div>

				<div class="form-group">
					<label for="first_name">First Name</label>
					<input type="text" value="{{ Auth::user()->first_name }}" name="first_name" class="form-control" id="first_name" placeholder="first name">
				</div>

				<div class="form-group">
					<label for="last_name">Last Name</label>
					<input type="text" value="{{ Auth::user()->last_name }}" name="last_name" class="form-control" id="last_name" placeholder="last name">
				</div>

				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" value="{{ Auth::user()->email }}" name="email" class="form-control" id="email" placeholder="email">
				</div>
			
				<button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
			</form>
		</div>
	</div>
</div>
<div class="col-lg-6">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Change Password<small class="m-l-sm">Use a minimum of 6 letter, number, & symbol combinations.</small></h5>
		</div>
		<div class="ibox-content">
			<form action="{{ route('password.update') }}" method="POST" role="form">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="old_password">Old Password</label>
					<input type="password" name="old_password" class="form-control" id="old_password" placeholder="Old password">
				</div>

				<div class="form-group">
					<label for="new_password">New Password</label>
					<input type="password" name="password" class="form-control" id="new_password" placeholder="New password">
				</div>

				<div class="form-group">
					<label for="password_confirmation">Confrim Password</label>
					<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password confirmation">
				</div>
			
				<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>
</div>
@endsection