@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Add Level<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route( 'levels.store', $category->id ) }}" method="POST" role="form">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="username">Name</label>
							<input type="text" value="{{ old('name') }}" name="name" class="form-control" id="username" placeholder="Name">
						</div>

						<div class="form-group">
							<label for="difficulty">Difficulty</label>
							<input type="number" value="{{ old('difficulty') }}" name="difficulty" class="form-control" id="difficulty" placeholder="1">
						</div>

						<button type="submit" class="btn btn-primary">Save Level <i class="fa fa-save"></i></button>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('levels', $category->id ) }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection