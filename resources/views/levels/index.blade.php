@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Levels<small class="m-l-sm">Manage all levels.</small></h5>
		</div>
		<div class="ibox-content">
			
			@if( $levels->count() )
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Difficulty</th>
							<th>Questions</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $levels as $level )

							<tr id="user-row-{{ $level->id }}">
								<td>{{ ucwords( str_replace("_", " ", $level->name ) ) }}</td>
								<td>{{ $level->difficulty }}</td>
								<td>
									<b>{{ $level->questions->count() }}</b> questions found.
									<a href="{{ route('questions.index', ['level'=>$level->id,'category'=>$category->id]) }}" class="btn pull-right btn-info btn-xs">
										<i class="fa fa-search"></i> View Questions
									</a>
								</td>
								<td>
									<a href="{{ route('levels.edit', $level->id) }}" class="btn btn-success btn-xs">
										<i class="fa fa-edit"></i> Edit
									</a>
									<button data-id="{{ $level->id }}" data-url="{{ route('levels.destroy', $level->id) }}" data-name="delete" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#remove-user">
										<i class="fa fa-close"></i> Delete
									</button>
								</td>
							</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			
			{{ $levels->links() }}
			@else
				
				<div class="alert alert-warning">
					<strong>No records!</strong> you may now start adding levels for this category.
				</div>

			@endif

		</div>
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove Level</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, all questions affiliated with this level will be deleted.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Cancel</button>
                <button type="button" id="remove-btn" class="btn btn-danger">Delete All</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route('levels.create', $category->id ) }}" class="btn btn-primary">Add level <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var level_id = button.data('id');
		jQuery('#remove-btn').data('id',level_id);
		jQuery('#remove-btn').data('url', button.data('url') );
		
  		jQuery(this).find('.modal-title').text('Remove ' + jQuery('#user-row-'+level_id+' td:first').html() );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var thisbtn = jQuery(this);
		var level_id = thisbtn.data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');
		
		jQuery.ajax({  
			url: thisbtn.data('url'),
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if ( data.error ) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+level_id).remove();
			}
		});
		 
	});

});
</script>
@endsection
