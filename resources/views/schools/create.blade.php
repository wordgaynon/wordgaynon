@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Add school<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route( 'schools.store' ) }}" method="POST" role="form">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="name">Name</label>
							<input type="text" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="Name">
						</div>

						<div class="form-group">
							<label for="address">Address</label>
							<input type="text" value="{{ old('address') }}" name="address" class="form-control" id="address" placeholder="">
						</div>

						<div class="form-group">
							<label for="phone">Phone</label>
							<input type="text" value="{{ old('phone') }}" name="phone" class="form-control" id="phone" placeholder="">
						</div>

						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" value="{{ old('email') }}" name="email" class="form-control" id="email" placeholder="">
						</div>


						<button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('schools.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection