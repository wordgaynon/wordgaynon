@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Details<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route( 'schools.update', $school->id ) }}" method="POST" role="form">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT">
				<div class="row">
					<div class="col-md-6">

						
						<div class="form-group">
							<label for="name">Name</label>
							<input type="text" value="{{ $school->name }}" name="name" class="form-control" id="name" placeholder="Name">
						</div>

						<div class="form-group">
							<label for="address">Address</label>
							<input type="text" value="{{ $school->address }}" name="address" class="form-control" id="address" placeholder="">
						</div>

						<div class="form-group">
							<label for="phone">Phone</label>
							<input type="text" value="{{ $school->phone }}" name="phone" class="form-control" id="phone" placeholder="">
						</div>

						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" value="{{ $school->email }}" name="email" class="form-control" id="email" placeholder="">
						</div>

						<button type="submit" class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('schools.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection

