@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Categories<small class="m-l-sm">Manage all category.</small></h5>
		</div>
		<div class="ibox-content">
			
			@if( $categories->count() )
			
			<form action="{{ route('category.index') }}" method="GET" class="form-inline" role="form">
				<div class="input-group">
					<input type="text" name="search" value="{{ $search }}" placeholder="Keyword" class="form-control">
					<span class="input-group-btn"> 
						<button type="submit" class="btn btn-info">Search <i class="fa fa-search"></i></button>
					</span>
				</div>
			</form>
			<hr>

			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Levels</th>
							<th>Questions</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach( $categories as $cat )

							<tr id="user-row-{{ $cat->id }}">
								<td>{{ $cat->name }} 
									<a href="{{ url("category/{$cat->id}/edit") }}" class="btn pull-right btn-success btn-xs">
										<i class="fa fa-edit"></i> Edit
									</a>
								</td>
								<td>There are <b>{{ $cat->levels()->count() }}</b> levels found
									<a href="{{ route('levels', $cat->id) }}" class="btn pull-right btn-info btn-xs">
										<i class="fa fa-edit"></i> Manage Levels
									</a>
								</td>
								<td>
									There are <b>{{ $cat->questions()->count() }}</b> questions found 
									<a href="{{ route('questions.index', ['category'=>$cat->id]) }}" class="btn pull-right btn-info btn-xs">
										<i class="fa fa-search"></i> View questions
									</a>
								</td>
								<td>
									<button data-id="{{ $cat->id }}" data-cat="{{ $cat->name }}" data-name="delete" class="btn pull-right btn-danger btn-xs" data-toggle="modal" data-target="#remove-user">
										<i class="fa fa-close"></i> Delete Category
									</button>
								</td>
							</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			
			{{ $categories->links() }}
			@else 

			<div class="alert alert-warning">
				<strong>No records!</strong> you may now start creating new categories.
			</div>

			@endif

		</div>
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove Category</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, all levels and questions affiliated with this category will be deleted. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Cancel</button>
                <button type="button" id="remove-btn" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route('category.create') }}" class="btn btn-primary">Add category <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var cat_id = button.data('id');
		jQuery('#remove-btn').data('id',cat_id);
  		jQuery(this).find('.modal-title').text('Remove ' + button.data('cat') );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var cat_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('category.index') }}/'+cat_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if ( data.error ) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+cat_id).remove();
			}
		});
		 
	});

});
</script>
@endsection
