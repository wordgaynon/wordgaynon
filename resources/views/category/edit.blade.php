@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Details<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route( 'category.update', $category->id ) }}" method="POST" role="form">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT">
				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="username">Name</label>
							<input type="text" value="{{ $category->name }}" name="name" class="form-control" id="username" placeholder="Name">
						</div>


						<button type="submit" class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
{{-- <a href="{{ route('roles.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a> --}}
</div>
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection

