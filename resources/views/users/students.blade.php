@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Student List<small class="m-l-sm">Manage all students.</small></h5>
		</div>
		<div class="ibox-content">
			
			@if( $students->count() )
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>Name</th>
							{{-- <th>Medals</th> --}}
							<th>Points</th>
							<th>School</th>
							<th>Device ID</th>
							<th>Statistics</th>
							<th>Certificate</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $students as $student )

							<tr>
								<td>{{ ucwords( str_replace("_", " ", $student->name ) ) }}</td>
								{{-- <td>{{ $student->medals }}</td> --}}
								<td>{{ $student->points }}</td>
								<td>{{ $student->school->name }}</td>
								<td>{{ $student->device_uuid }}</td>
								<td><button class="btn btn-xs btn-info" data-toggle="modal" href='#statistics' data-stats="[{{ json_encode($student->datas) }}]">View Stats</button></td>
								<td><a href="{{ route( 'certificate', $student->id ) }}" class="btn btn-xs btn-info" target="_blank">Certificate</a></td>
							</tr>

							<!-- [{!! json_encode($student->datas) !!}] -->
						@endforeach
					</tbody>
				</table>
			</div>
			
			{{ $students->links() }}
			@else
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>No records</strong>, there are no available students at the moment.
			</div>
			@endif

		</div>
	</div>
</div>
@endsection

@section('action')
{{-- <div class="title-action">
    <a href="{{ route('users.create') }}" class="btn btn-primary">Add User <i class="fa fa-plus"></i></a>
</div> --}}
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	jQuery('#statistics').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var stats = button.data('stats'); // Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this);
		// modal.find('.modal-title').text('New message to ' + recipient)
		// modal.find('.modal-body input').val(recipient)
		var html = '';

		if ( stats != '["null"]' ) {
			
			var statistics = jQuery.parseJSON( stats );
			
			console.log( statistics );

			for ( var key in statistics ) {
				if ( statistics.hasOwnProperty( key ) ) {
					var catname = key;

					for ( var level in statistics[key] ) {

						if ( statistics[key].hasOwnProperty( level ) ) {
							var levelname = level;

							for ( var item in statistics[key][level] ) {
								if ( statistics[key][level].hasOwnProperty( item ) ) {
									
									html += '<tr>';
									html += '<td>'+catname+'</td>';
									html += '<td>'+levelname+'</td>';
									html += '<td>'+statistics[key][level][item].name+'</td>';
									html += '<td>'+statistics[key][level][item].correct+'</td>';
									html += '<td>'+statistics[key][level][item].wrong+'</td>';
									html += '<td>'+ parseFloat( statistics[key][level][item].wrong + statistics[key][level][item].correct ) +'</td>';
									html += '</tr>';
								}
							}
						}
					}
				}
			}
			
			modal.find('tbody').html( html );

		}

	});

	function returnObject( obj ) {
		for ( var key in obj ) {

		}
	}
});
</script>

<div class="modal fade" id="statistics">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Statistics</h4>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-hover" style="text-align: center;">
						<thead>
							<tr>
								<th style="text-align: center;">Category</th>
								<th style="text-align: center;">Level</th>
								<th style="text-align: center;">Question</th>
								<th style="text-align: center;">Correct</th>
								<th style="text-align: center;">Wrong</th>
								<th style="text-align: center;">Attempts</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endsection
