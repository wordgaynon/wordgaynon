@extends('layouts.admin')



@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Add User<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route('users.store') }}" method="POST" role="form">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" value="{{ old('email') }}" name="email" class="form-control" id="email" placeholder="email">
						</div>

						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" value="{{ old('name') }}" name="name" class="form-control" id="username" placeholder="username">
						</div>

						<div class="form-group">
							<label for="first_name">First Name</label>
							<input type="text" value="{{ old('first_name') }}" name="first_name" class="form-control" id="first_name" placeholder="first name">
						</div>

						<div class="form-group">
							<label for="last_name">Last Name</label>
							<input type="text" value="{{ old('last_name') }}" name="last_name" class="form-control" id="last_name" placeholder="last name">
						</div>
						
						@if( $schools->count() )
						<div class="form-group">
							<label for="school">School</label>
							<select name="school" class="form-control" id="school" disabled="">
								@foreach( $schools as $school )
								<option value="{{ $school->id }}"{{ old('school') == $school->id ? ' selected':'' }}>{{ ucwords( $school->name ) }}</option>
								@endforeach
							</select>
						</div>
						@endif
						
					</div>
					<div class="col-md-6">

						<div class="form-group">
							<label for="new_password">Password</label>
							<input type="password" name="password" class="form-control" id="new_password" placeholder="Password">
						</div>

						<div class="form-group">
							<label for="password_confirmation">Repeat Password</label>
							<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Repeat Password">
						</div>

						<label for="user_role">User Role</label>

						@if( $roles )
						@foreach( $roles as $role )
						
						<div class="form-group">
							<div class="checkbox i-checks">
								<label> <input type="checkbox" name="user_role[]" data-name="{{ $role->name }}" value="{{ $role->id }}"><i></i> {{ ucfirst( $role->name ) }} </label>
							</div>
						</div>
						
						@endforeach
						@endif
						
						<button type="submit" class="btn btn-primary">Save User <i class="fa fa-save"></i></button>
					</div>
				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('users.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection


@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        jQuery(document).ready(function () {
            jQuery('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            jQuery('.i-checks').on('ifChecked', function(event){
				if( jQuery(this).find('input').data('name') == 'teacher' ){
					jQuery('#school').removeAttr('disabled');
				}
			});

			jQuery('.i-checks').on('ifUnchecked', function(event){
				if( jQuery(this).find('input').data('name') == 'teacher' ){
					jQuery('#school').attr('disabled','disabled');
				}
			});
        });
    </script>
@endsection