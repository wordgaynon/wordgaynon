<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
    ];

    public function questions()
    {
    	return $this->hasMany('App\Question', 'category_id', 'id');
    }

    public function levels()
    {
    	return $this->hasMany('App\Level', 'category_id', 'id');
    }
}
