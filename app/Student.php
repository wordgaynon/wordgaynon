<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
       'name', 'medals', 'points', 'datas', 'device_uuid'
    ];

    public function teacher()
    {
		return $this->hasOne('App\User', 'id', 'teacher_id');
    }

    public function school()
    {
		return $this->hasOne('App\School', 'id', 'school_id');
    }
}
