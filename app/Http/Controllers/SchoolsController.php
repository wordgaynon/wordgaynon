<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use App\User;
use Validator;
use Auth;

class SchoolsController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Schools',
            'description' => 'Manage all schools.',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->params['schools'] = School::paginate(10);

        return view('schools.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->params['title'] = 'Register new shcool';
        $this->params['description'] = 'Customize and register new school.';
        return view('schools.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $validate = array(
            'name' => 'required|string|max:255',
            'address' => 'string|max:255',
            'phone' => 'string|max:255',
            'email' => 'string|email|max:255',
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('schools/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $school = new School();
        $school->fill( $request->all() );
        $school->save();

        return redirect('schools')->with('success', 'School successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit school info';
        $this->params['description'] = 'Update school details.';

        $school = School::find( $id );

        if ( ! $school ) {
            return redirect('schools')->with('warning', 'School no longer exist.');
        }

        $this->params['school'] = $school;

        return view('schools.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $validate = array(
            'name' => 'required|string|max:255',
            'address' => 'string|max:255',
            'phone' => 'string|max:255',
            'email' => 'string|email|max:255',
        );

        $school = School::find( $id );

        // double check if user exist.
        if ( ! $school ) {
            return redirect('schools')->with('error', 'School does not exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('schools/'. $school->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $school->fill( $request->all() );
        $school->save();

        return redirect('schools/'. $school->id .'/edit')->with('success', 'School details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin']);

        $school = School::find( $id );

        if ( ! $school ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        $school->delete();

        return response()->json([
            'error' => false,
            'message' => 'level successfuly removed.'
        ]);
    }
}
