<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use Validator;
use App\School;
use App\User;
use App\Role;
use Auth;

class UsersController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Users',
            'description' => 'Manage all users and roles.',
            'roles' => Role::all(),
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->params['users'] = User::paginate(10);

        return view('users.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->params['title'] = 'Register new users';
        $this->params['description'] = 'Customize and register new user account.';
        $this->params['schools'] = School::all();

        return view('users.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles('admin');

        $validate = array(
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'password' => 'required|confirmed|min:6|max:50',
            'password_confirmation' => 'required',
            'user_role' => 'required|array',
        );

        if ( $request->input('school') ) {
            $validate['school'] = 'integer';
        }
        
        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('users/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $user = new User();
        $user->fill( $request->all() );
        $user->password = bcrypt( $request->input('passwod') );
        if ( $request->input('school') ) { $user->school_id = $request->input('school'); }
        $user->save();

        // attach user roles.
        foreach ( $request->input('user_role') as $role_id ) {
            $user->roles()->attach( Role::find( $role_id ) );    
        }

        return redirect('users')->with('success', 'User ' . $user->name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $this->params['title'] = 'Edit User Info';
        $this->params['description'] = 'Update user information.';
        $this->params['schools'] = School::all();
        
        $user = User::find( $id );

        if ( ! $user ) {
            return redirect('users')->with('warning', 'User no longer exist.');
        }

        $roles = array();
        foreach ( $user->roles()->get() as $role ) {
            $roles[] = $role->id;
        }

        $this->params['user'] = $user;
        $this->params['user_roles'] = $roles;

        return view('users.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {

        // Block users who are not admin
        $request->user()->authorizeRoles('admin');

        $validate = array(
            'name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'user_role' => 'required|array',
        );

        $user = User::find( $id );

        // double check if user exist.
        if ( ! $user ) {
            return redirect('users')->with('error', 'User does not exist, please try again.');
        }

        // check user email if changed.
        if ( $user->email != $request->input('email') ) {
            $validate['email'] = 'required|string|email|max:255|unique:users';
        }

        if ( $request->input('school') ) {
            $validate['school'] = 'integer';
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('users/'. $user->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $user->fill( $request->all() );
        if ( $request->input('school') ) { $user->school_id = $request->input('school'); }
        $user->save();

        // Remove all user roles
        $user->roles()->detach();        

        // attach user roles.
        foreach ( $request->input('user_role') as $role_id ) {
            $user->roles()->attach( Role::find( $role_id ) );    
        }

        return redirect('users/'. $user->id .'/edit')->with('success', 'User details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles('admin');

        $user = User::find( $id );

        if ( ! $user ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        } 

        $user->roles()->detach();
        $user->delete();

        return response()->json([
            'error' => false,
            'message' => 'User successfuly removed.'
        ]);
    }
}
 