<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Category;
use App\Question;
use App\Level;
use App\User;
use Auth;
use Image;
use File;

class QuestionsController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Questions',
            'description' => 'Manage all questions.',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $perpage = ( preg_replace( '/\D/', '', $request->input('perpage') ) ) ? $request->input('perpage'): 10;
        $category = ( preg_replace( '/\D/', '', $request->input('category') ) ) ? $request->input('category'): '';
        $level = ( preg_replace( '/\D/', '', $request->input('level') ) ) ? $request->input('level'): '';
        $search = ( $request->input('search') ) ? $request->input('search'): '';

        $this->params['perpage'] = $perpage;
        $this->params['category_id'] = $category;
        $this->params['level_id'] = $level;
        $this->params['search'] = $search;

        if ( Auth::user()->hasRole('admin') ) {

            $questions = Question::orderBy('id','desc');    
            $this->params['categories'] = Category::all();
            $this->params['levels'] = Level::all();

        } else {

            $questions = Question::where( 'teacher_id', '=', Auth::user()->id )->orderBy('id','desc');
            $this->params['categories'] = Category::where( 'user_id', '=', Auth::user()->id )->get();
            $this->params['levels'] = Level::where( 'user_id', '=', Auth::user()->id )->orderBy('difficulty','desc')->get();
        }

        if ( $category ) { $questions->where('category_id','=',$category); }
        if ( $level ) { $questions->where('level_id','=',$level); }
        if ( $search ) { $questions->where('name', 'LIKE', "%$search%"); }


        $this->params['questions'] = $questions->paginate($perpage);

        return view('questions.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->params['title'] = 'Create new question';
        $this->params['description'] = 'Customize and register new question.';

        if ( Auth::user()->hasRole('admin') ) {
            $this->params['categories'] = Category::all();
            $this->params['levels'] = Level::orderBy('difficulty','desc')->get();
        } else {
            $this->params['categories'] = Category::where( 'user_id', '=', Auth::user()->id)->get();
            $this->params['levels'] = Level::where( 'user_id', '=', Auth::user()->id)->orderBy('difficulty','desc')->get();
        }

        return view('questions.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['teacher', 'admin']);

        $validate = array(
            'name' => 'required|string|max:10',
            'image' => 'required|mimes:png,jpg,jpeg',
            'category' => 'required|integer',
            'level' => 'required|integer',
        );

        if ( $request->hasFile('sound') ) {
            $validate['sound'] = 'mimes:mpga,wav';
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('questions/create')
                ->withErrors( $validator )
                ->withInput();
        }

        if ( $request->hasFile('image') ) {
            if ( !$request->file('image')->isValid() ) {
                return redirect('questions/create')->with('error', 'The photo you uploaded is invalid.');
            }
        } else {
            return redirect('questions/create')->with('error', 'Select a photo for upload.');
        }

        $category = Category::find( $request->input('category') );
        $level = Level::find( $request->input('level') );

        if ( ! $category ) {
            return redirect('questions/create')->with('error', 'Category no longer exist.')->withInput();
        }

        if ( ! $level ) {
            return redirect('questions/create')->with('error', 'Level no longer exist.')->withInput();
        }

        if ( Question::where([
                ['name',        '=', strtolower( $request->input('name') )],
                ['category_id', '=', $category->id],
                ['level_id',    '=', $level->id],
            ])->count() ) {
            return redirect('questions/create')->with('error', 'Question already exist.')->withInput();
        }

        $userpath = '/uploads/' . Auth::user()->id . '/' . strtolower( $category->name ) . '/' . strtolower( $level->name ) . '/'; 
        $directory = public_path() . $userpath;
        $filename = strtolower( $request->input('name') ) . '.' . $request->image->extension();
        $upload_path = $directory . $filename;

        // check if direcotry exist
        if ( ! is_dir( $directory ) ) {
            // Create directory if it does not exist.
            File::makeDirectory( $directory, $mode = 0777, true, true );
        }

        if ( $request->hasFile('sound') ) {
            $mp3 = $request->file('sound');
            $mp3_name = strtolower( $request->input('name') ) . '.' . $mp3->getClientOriginalExtension();
            //Move Uploaded File
            $mp3->move( $directory, $mp3_name );
        }

        // Save image
        Image::make( $request->image )->resize(300, 200)->save( $upload_path );

        $question = new Question();
        $question->fill( $request->all() );
        $question->name = strtolower( $request->input('name') );
        $question->image = $filename;
        $question->sound = ( $request->hasFile('sound') ) ? $mp3_name:'';
        $question->category_id = $request->input('category');
        $question->level_id = $request->input('level');
        $question->teacher_id = Auth::user()->id;
        $question->save();

        return redirect('questions')->with('success', 'Question successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit Question';
        $this->params['description'] = 'Update question details.';

        $question = Question::find( $id );

        if ( ! $question ) {
            return redirect('questions')->with('warning', 'Question no longer exist.');
        }

        $this->params['question'] = $question;

        if ( Auth::user()->hasRole('admin') ) {
            $this->params['categories'] = Category::all();
            $this->params['levels'] = Level::orderBy('difficulty','desc')->get();
        } else {
            $this->params['categories'] = Category::where( 'user_id', '=', Auth::user()->id)->get();
            $this->params['levels'] = Level::where( 'user_id', '=', Auth::user()->id)->orderBy('difficulty','desc')->get();
        }

        return view('questions.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['teacher', 'admin']);

        $validate = array(
            'name' => 'required|string|max:10',
            'category' => 'required|integer',
            'level' => 'required|integer',
        );

        $question = Question::find( $id );

        // double check if user exist.
        if ( ! $question ) {
            return redirect('questions')->with('error', 'question does not exist, please try again.');
        }

        if ( $request->hasFile('sound') ) {
            $validate['sound'] = 'mimes:mpga,wav';
        }

        if ( $request->hasFile('image') ) {
            $validate['image'] = 'mimes:png,jpg,jpeg';
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('questions/'. $question->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $category = Category::find( $request->input('category') );
        $level = Level::find( $request->input('level') );

        if ( ! $category ) {
            return redirect('questions/' . $question->id . '/edit')->with('error', 'Category no longer exist.')->withInput();
        }

        if ( ! $level ) {
            return redirect('questions/' . $question->id . '/edit')->with('error', 'Level no longer exist.')->withInput();
        }

        $newname = strtolower( $request->input('name') );

        if ( $newname != $question->name ) {
            if ( Question::where([
                    ['name',        '=', $newname],
                    ['category_id', '=', $category->id],
                    ['level_id',    '=', $level->id],
                ])->count() ) {
                return redirect('questions/' . $question->id . '/edit')->with('error', 'Question already exist.')->withInput();
            }
        }

        $userpath = '/uploads/' . $question->teacher_id . '/' . strtolower( $category->name ) . '/' . strtolower( $level->name ) . '/'; 
        $directory = public_path() . $userpath;

        if ( $request->hasFile('image') ) {

            if ( ! $request->file('image')->isValid() ) {
                return redirect('questions/' . $question->id . '/edit')->with('error', 'The photo you uploaded is invalid.');
            }

            $filename = $newname . '.' . $request->image->extension();
            $upload_path = $directory . $filename;
        }



        // check if direcotry exist
        if ( ! is_dir( $directory ) ) {
            // Create directory if it does not exist.
            File::makeDirectory( $directory, $mode = 0777, true, true );
        }

        if ( $request->hasFile('sound') ) {

            $mp3 = $request->file('sound');
            $mp3_name = $newname . '.' . $mp3->getClientOriginalExtension();

            // check if file exist
            if ( File::exists( $directory . $question->sound ) ) {
                // Delete old mp3 file.
                File::delete( $directory . $question->sound );
            }

            //Move Uploaded File
            $mp3->move( $directory, $mp3_name );
        }

        // Save new image
        if ( $request->hasFile('image') ) {

            // delete old image file
            if ( File::exists( $directory . $question->image ) ) {
                File::delete( $directory . $question->image );
            }

            // save new image file
            Image::make( $request->image )->resize(300, 200)->save( $upload_path );

        }

        // Rename files if name is changed. 
        if ( $question->sound ) {
            $sound_arr = explode('.', $question->sound);
            // if ( $sound_arr[0] != $newname ) {
                $oldSoundName = $sound_arr[0] . '.'. $sound_arr[1];
                if ( File::exists( $directory . $oldSoundName ) ) {
                    File::move( $directory . $oldSoundName, $directory . $newname . '.'. $sound_arr[1] );
                    $question->sound = $newname . '.'. $sound_arr[1];
                }
            // }
        }

        // Rename files if name is changed. 
        if ( $question->image ) {
            $image_arr = explode('.', $question->image);
            // if ( $image_arr[0] != $newname ) {
                $oldImageName = $image_arr[0] . '.'. $image_arr[1];
                if ( File::exists( $directory . $oldImageName ) ) {
                    File::move( $directory . $oldImageName, $directory . $newname . '.'. $image_arr[1] );
                    $question->image = $newname . '.'. $image_arr[1];
                }
            // }
        }

        if ( $request->hasFile('image') ) { $question->image = $filename; }
        if ( $request->hasFile('sound') ) { $question->sound = $mp3_name; }

        if ( ! File::exists( $directory . $question->image ) ) {
            $question->image = '';
        }

        if ( ! File::exists( $directory . $question->sound ) ) {
            $question->sound = '';
        }

        $question->name = $newname;
        $question->category_id = $request->input('category');
        $question->level_id = $request->input('level');
        // dd( $question );
        $question->save();

        return redirect('questions/'. $question->id .'/edit')->with('success', 'Question details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['teacher', 'admin']);

        $question = Question::find( $id );

        if ( ! $question ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        $userpath = '/uploads/' . $question->teacher_id . '/' . strtolower( $question->category->name ) . '/' . strtolower( $question->level->name ) . '/'; 
        $directory = public_path() . $userpath;

        if ( File::exists( $directory . $question->image ) ) {
            File::delete( $directory . $question->image );
        }

        if ( File::exists( $directory . $question->sound ) ) {
            File::delete( $directory . $question->sound );
        }

        $question->delete();

        return response()->json([
            'error' => false,
            'message' => 'Question successfuly removed.'
        ]);
    }
}
