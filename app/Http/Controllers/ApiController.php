<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Question;
use App\Category;
use App\Student;
use App\Level;
use App\Role;
use App\User;
use App\Stat;
use Auth;
use File;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->params = array(
            'error' => false,
            'message' => '',
            'data' => null,
        );
    }

    /**
     * Shows teachers.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_admin()
    {
        
        $this->params['data'] = User::whereHas('roles', function($q){
            $q->where('name', 'admin');
        })->first();

        return response()->json( $this->params );
    }

    /**
     * Shows teachers.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_teachers()
    {
    	
        $this->params['data'] = User::whereHas('roles', function($q){
            $q->where('name', 'teacher');
        })->get();

        return response()->json( $this->params );
    }

    /**
     * Pull students.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_students()
    {
         
        $this->params['data'] = User::whereHas('roles', function($q){
            $q->where('name', 'student');
        })->get();

        return response()->json( $this->params );
    }

    /**
     * Shows teachers.
     *
     * @return \Illuminate\Http\Response
     */
    public function newstudent( Request $request )
    {

        $teacher_id = $request->input('teacher');
        $teacher = User::find($teacher_id);
        $student_name = strtolower( str_replace(" ", "_", $request->input('student') ) );

        if ( !$teacher ) {
            $this->params['error'] = true;
            $this->params['message'] = 'Please try again.';
            return response()->json( $this->params );
        }

        $student = Student::where([
            ['name', '=', $student_name],
            ['teacher_id', '=', $teacher_id],
        ])->first();

        $categories = Category::where( 'user_id', '=', $teacher_id )->get();
        $category = array();
        $completed = array();
        $categoryNames = array();
        foreach ($categories as $cat) {
            $category[$cat->name] = array(
                'title' => ucwords( $cat->name ),
                'icon' => '',
                'background' => '',
                'data' => $this->get_levels( $cat->id, $teacher_id ),
            );

            $completed[$cat->name] = $this->get_levels( $cat->id, $teacher_id, false );
            $categoryNames[$cat->name] = false;
        }

        $categoryNames['panghangkat'] = false;
        
        $category['panghangkat'] = array(
            'title' => 'Panghangkat',
            'icon' => '',
            'background' => '',
            'data' => $this->get_wildcards( $teacher_id ),
        );

        $completed['panghangkat'] = array('mabudlay' => false);

        if ( $student ) {
            $this->params['data'] = array(
                'finished' => 0,
                'category' => $categoryNames,
                'completed' => $completed,
                'data' => $category,
                'student_id' => $student->id,
            );
            return response()->json( $this->params );
        }

        $new_student = new Student();
        $new_student->name = $student_name;
        $new_student->datas = json_encode( $this->params['data'] );
        $new_student->device_uuid = $request->input('device');
        $new_student->teacher_id = $teacher->id;
        $new_student->school_id = $teacher->school->id;
        $new_student->save();

        $this->params['data'] = array(
            'finished' => 0,
            'category' => $categoryNames,
            'completed' => $completed,
            'data' => $category,
            'student_id' => $new_student->id,
        );

        return response()->json( $this->params );
    }

    public function savestudent( Request $request )
    {

        $studentid     = $request->input('student_id');
        $device_id     = $request->input('device');
        $player_name   = $request->input('player');
        $points        = $request->input('points');
        $medals        = $request->input('medals');
        $category_name = $request->input('category');
        $level_name    = $request->input('level');
        $item          = $request->input('item');
        $wrong         = $request->input('wrong');
        $correct       = $request->input('correct');

        if ( !$studentid ) {
            $this->params['error'] = true;
            $this->params['message'] = 'Student id empty';
            return response()->json( $this->params );
        }

        $student = Student::find( $studentid );

        if ( !$student ) {
            $this->params['error'] = true;
            $this->params['message'] = 'Student not found';
            return response()->json( $this->params );
        }

        if ( $points ) { $student->points = $points; }
        if ( $medals ) { $student->medals = $medals; }
        
        $item = Question::find( $item );

        if ( $item ) {
            
            $item->wrong = $wrong;
            $item->correct = $correct;

            $level[$item->id] = $item;
            $category[$category_name] = $level;
            $stats[$category_name] = $category;

            $student->datas = json_encode( $stats );
        }

        $student->save();

        $this->params['data'] = $student;
        $this->params['message'] = 'Student data updated.';
        return response()->json( $this->params );

    }

    /**
     * Pull questions for every levels.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_levels( $category_id, $teacher_id, $is_name = true )
    {
        $levels = Level::where([
            [ 'category_id', '=', $category_id ],
            [ 'user_id', '=', $teacher_id ],
        ])->orderBy('difficulty','asc')->get();

        $data = array();
        $challenge = array();

        foreach ($levels as $level) {
            if ( $is_name ) {
                $data[$level->name] = $level->questions()->get();   
            } else {
                $data[$level->name] = false;
            }
        }
        
        return $data;
    }

    /**
     * Pull questions for every levels.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_wildcards( $teacher_id )
    {
        $levels = Level::where([
            [ 'user_id', '=', $teacher_id ]
        ])->orderBy('difficulty','asc')->get();

        $data = array();
        $challenge = array();

        foreach ($levels as $level) {
            $q = $level->questions()->get();   
            foreach ( $q as $val ) {
                
                $category = Category::find( $val->category_id );
                $level = Level::find( $val->level_id );

                $val->category = $category->name;
                $val->level = $level->name;

                $data[] = $val;
            }
        }

        $challenge['mabudlay'] = $data;
        
        return $challenge;
    }
}
