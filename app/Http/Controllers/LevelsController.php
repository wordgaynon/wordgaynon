<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Question;
use App\Category;
use App\Level;
use App\User;
use Auth;
use File;

class LevelsController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Levels',
            'description' => 'Manage all levels.',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $category_id )
    {

        $cat = Category::find( $category_id );

        if ( ! $cat ) {
            return redirect('category')->with('warning', 'Category no longer exist.');
        }

        $this->params['title'] = 'Manage levels for ' . $cat->name . ' category';
        $this->params['levels'] = Level::where('category_id','=', $category_id)->paginate(10);
        $this->params['category'] = $cat;

        return view('levels.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $category_id )
    {
        $cat = Category::find( $category_id );

        if ( ! $cat ) {
            return redirect('category')->with('warning', 'Category no longer exist.');
        }

        $this->params['title'] = 'Create new level for ' . $cat->name . ' category';
        $this->params['description'] = 'Customize and register new level.';
        $this->params['category'] = $cat;

        return view('levels.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $category_id)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['teacher', 'admin']);

        if ( ! Category::find( $category_id ) ) {
            return redirect('category')->with('warning', 'Category no longer exist.');
        }

        $validate = array(
            'name' => 'required|string|max:255',
            'difficulty' => 'required|integer',
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('levels/'. $category_id .'/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $level = new Level();
        $level->fill( $request->all() );
        $level->name = strtolower( str_replace(" ", "_", $request->input('name') ) );
        $level->category_id = $category_id;
        $level->user_id = Auth::user()->id;
        $level->save();

        return redirect('levels/'.$category_id)->with('success', 'Level ' . $level->name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit Level';
        $this->params['description'] = 'Update level details.';

        $level = Level::find( $id );

        if ( ! $level ) {
            return redirect('levels')->with('warning', 'Level no longer exist.');
        }

        $this->params['level'] = $level;

        return view('levels.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['teacher', 'admin']);

        $validate = array(
            'name' => 'required|string|max:255',
            'difficulty' => 'required|integer',
        );

        $level = Level::find( $id );

        // double check if user exist.
        if ( ! $level ) {
            return redirect('levels')->with('error', 'Level does not exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('levels/'. $level->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $level->fill( $request->all() );
        $level->name = strtolower( str_replace(" ", "_", $request->input('name') ) );
        $level->save();

        return redirect('levels/'. $level->id .'/edit')->with('success', 'Level details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['teacher', 'admin']);

        $level = Level::find( $id );

        if ( ! $level ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        // delete all questions
        $questions = Question::where('level_id','=',$level->id)->get();
        foreach ($questions as $question) {
            $userpath = '/uploads/' . $question->teacher_id . '/' . strtolower( $question->category->name ) . '/' . strtolower( $question->level->name ) . '/'; 
            $directory = public_path() . $userpath;
            if ( File::exists( $directory . $question->image ) ) { File::delete( $directory . $question->image ); }
            if ( File::exists( $directory . $question->sound ) ) { File::delete( $directory . $question->sound ); }
            $question->delete();
        }

        $level->delete();

        return response()->json([
            'error' => false,
            'message' => 'level successfuly removed.'
        ]);
    }
}
