<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Question;
use App\Category;
use App\level;
use App\User;
use Auth;
use File;

class CategoryController extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Categories',
            'description' => 'Manage all categories.',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $search = ( $request->input('search') ) ? $request->input('search'): '';
        $this->params['search'] = $search;

        if ( Auth::user()->hasRole('admin') ) {
            $categories = Category::orderBy('id','desc');
        } else {
            $categories = Category::where('user_id', '=', Auth::user()->id)->orderBy('id','desc');
        }

        if ( $search ) { $categories->where('name', 'LIKE', "%$search%"); }

        $this->params['categories'] = $categories->paginate(10);

        return view('category.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->params['title'] = 'Create new category';
        $this->params['description'] = 'Customize and register new category.';

        return view('category.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['teacher', 'admin']);

        $validate = array(
            'name' => 'required|string|max:255',
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('category/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $category = new Category();
        $category->name = strtolower( str_replace(" ", "_", $request->input('name') ) );
        $category->user_id = Auth::user()->id;
        $category->save();

        return redirect('category')->with('success', 'Category ' . $category->name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit Category';
        $this->params['description'] = 'Update category details.';

        $category = Category::find( $id );

        if ( ! $category ) {
            return redirect('category')->with('warning', 'Category no longer exist.');
        }

        $this->params['category'] = $category;

        return view('category.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['teacher', 'admin']);

        $validate = array(
            'name' => 'required|string|max:255',
        );

        $category = Category::find( $id );

        // double check if user exist.
        if ( ! $category ) {
            return redirect('category')->with('error', 'Category does not exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('categorys/'. $category->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $category->name = strtolower( str_replace(" ", "_", $request->input('name') ) );
        $category->save();

        return redirect('category/'. $category->id .'/edit')->with('success', 'Category details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['teacher', 'admin']);

        $category = Category::find( $id );

        if ( ! $category ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        // delete all questions
        $questions = Question::where('category_id','=',$category->id)->get();
        foreach ($questions as $question) {
            $userpath = '/uploads/' . $question->teacher_id . '/' . strtolower( $question->category->name ) . '/' . strtolower( $question->level->name ) . '/'; 
            $directory = public_path() . $userpath;
            if ( File::exists( $directory . $question->image ) ) { File::delete( $directory . $question->image ); }
            if ( File::exists( $directory . $question->sound ) ) { File::delete( $directory . $question->sound ); }
            $question->delete();
        }

        // delete all levels
        $levels = Level::where('category_id','=',$category->id)->get();
        foreach ($levels as $level) {
            $level->delete();
        }

        // Delete category
        $category->delete();

        return response()->json([
            'error' => false,
            'message' => 'Category successfuly removed.'
        ]);
    }
}
