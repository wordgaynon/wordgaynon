<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Auth;
use Hash;

class SettingsController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->params = array(
            'title' => 'Settings',
            'description' => 'Where all user settings are being managed.',
            'success_message' => '',
            'error_message' => '',
        );

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.index', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile_update(Request $request)
    {
        
        $validate = array(
            'name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255'
        );

        $user = User::find( Auth::user()->id );

        // double check if user exist.
        if ( ! $user ) {
            return redirect('settings')->with('error', 'Please try again.');
        }

        // check user email if changed.
        if ( Auth::user()->email != $request->input('email') ) {
            $validate['email'] = 'required|string|email|max:255|unique:users';
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('settings')
                ->withErrors($validator)
                ->withInput();
        }

        $user->fill( $request->all() );
        $user->save();

        return redirect('settings')->with('success', 'Profile successfuly updated.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function password_update(Request $request)
    {
        
        $validate = array(
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6|max:50|different:old_password',
            'password_confirmation' => 'required',
        );

        $user = User::find( Auth::user()->id );

        // double check if user exist.
        if ( ! $user ) {
            return redirect('settings')->with('error', 'Please try again.');
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('settings')
                ->withErrors($validator)
                ->withInput();
        }

        if ( ! Hash::check( $request->input('old_password'), $user->password ) ) {
            return redirect('settings')->with('error', 'Your old password did not match.');
        }

        $user->password = bcrypt( $request->input('password') );
        $user->save();

        return redirect('settings')->with('success', 'You have updated your password successfuly.');
    }
}
