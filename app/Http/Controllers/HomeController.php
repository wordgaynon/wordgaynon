<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->params = array(
            'title' => 'Dashboard',
            'description' => 'Deafault landing page for admin',
        );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard', $this->params);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function students()
    {

        if ( Auth::user()->hasRole('admin') ) {
            $student = Student::paginate(50);
        } else {
            $student = Student::where('school_id','=',Auth::user()->school_id)->paginate(50);
        }

        $this->params['students'] = $student;

        return view('users.students', $this->params);
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function certificate( $id )
    {    

        $student = Student::find($id);
        $this->params['student'] = $student;

        return view('users.certificate', $this->params);
    }
}
