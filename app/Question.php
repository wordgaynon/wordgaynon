<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'name', 'image', 'sound'
    ];

    public function category()
    {
    	return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function level()
    {
    	return $this->hasOne('App\Level', 'id', 'level_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'teacher_id');
    }
}
