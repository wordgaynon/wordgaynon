<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
        'name', 'address', 'phone', 'email'
    ];

    public function user()
    {
		return $this->hasOne('App\User', 'school_id', 'id');
    }

    public function students()
    {
		return $this->hasMany('App\Student', 'school_id', 'id');
    }
}
