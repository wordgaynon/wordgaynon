<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $fillable = [
        'name', 'difficulty'
    ];

    public function category()
    {
    	return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function questions()
    {
    	return $this->hasMany('App\Question', 'level_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
