<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Role comes before User seeder here.
        $this->call(RoleTableSeeder::class);
        // Register Shcools
    	$this->call(SchoolsTableSeeder::class);
		// User seeder will use the roles above created.
    	$this->call(UserTableSeeder::class);
        // Create default categories
        $this->call(CategoryTableSeeder::class);
        // populate category levels
        $this->call(LevelsTableSeeder::class);
        // populate questiibs levels
        $this->call(QuestionTableSeeder::class);
    }
}
