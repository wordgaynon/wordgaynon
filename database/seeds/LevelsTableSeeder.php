<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Level;
use App\User;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$category = Category::all();
    	$teacherid = User::where('name','=','Teacher')->first()->id;

    	foreach ($category as $cat) {

    		$easy = new Level();
	        $easy->name = 'mahulas';
	        $easy->difficulty = 1;
	    	$easy->category_id = $cat->id;
	    	$easy->user_id = $teacherid;
	    	$easy->save();

	    	$medium = new Level();
	        $medium->name = 'medyo_mahulas';
	        $medium->difficulty = 2;
	    	$medium->category_id = $cat->id;
	    	$medium->user_id = $teacherid;
	    	$medium->save();

	    	$hard = new Level();
	        $hard->name = 'mabudlay';
	        $hard->difficulty = 3;
	    	$hard->category_id = $cat->id;
	    	$hard->user_id = $teacherid;
	    	$hard->save();
    	}
    	
    }
}
