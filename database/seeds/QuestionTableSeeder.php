<?php

use Illuminate\Database\Seeder;
use App\Question;
use App\Category;
use App\Level;
use App\User;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $lawas = array(
			'mahulas'   => array('ulo','paa','tiil','mata','baba','ilok','kuko','titi','dahi','guya'),
			'medyo_mahulas' => array('apdo','baga','buli','lasi','liog','tiyan','ugat','bato','buol','kamot'),
			'mabudlay'   => array('abaga','batiis','bibig','buhok','bungot','butkon','dughan','hawak','ilong' )
		);

		$this->looper( 'lawas', $lawas );

		$butang = array(
			'mahulas'   => array('lata','baga','suga','bayo','bato','tasa','yabi','ulan','baso','bola'),
			'medyo_mahulas' => array('paha','relo','nipa','sulo','ugat','hilo','gata','kalo','kabo','saya'),
			'mabudlay'   => array('husay','dahon','balay','libro','lapis','katre','panyo','papel','bulak','uling'),
		);

		$this->looper( 'butang', $butang );

		$sapat = array(
			'mahulas'   => array('amo','igi','bao','ido','pato','paka','isda','tiki','ulod','baka'),
			'medyo_mahulas' => array('anay','apan','halo','iras','lago','tuko','pisu','subay','baboy','ilaga'),
			'mabudlay'   => array('manok','gansa','lamok','tanga','pispis','kuring','pating','koneho','langaw','karabaw'),
		);

		$this->looper( 'sapat', $sapat );

		$tanum = array(
			'mahulas'   => array('mani','okra','ahos','iba','niyog','rosas','tambo','atis','mais'),
			'medyo_mahulas' => array('humay','babana','saging','suha','pinya','tsiko','nipa','paho','mangka'),
			'mabudlay'   => array('kabugaw','kamonsil','lumboy','bayabas','sambag','sandiya','kamote','singkamas'),
		);

		$this->looper( 'tanum', $tanum );
    }

    public function looper( $category, $data )
    {
    	$cat      = Category::where( 'name', '=', $category)->first();
    	$mahulas  = Level::where([ ['category_id', '=', $cat->id], ['difficulty', '=', 1] ])->first();
    	$medyo    = Level::where([ ['category_id', '=', $cat->id], ['difficulty', '=', 2] ])->first();
    	$mabudlay = Level::where([ ['category_id', '=', $cat->id], ['difficulty', '=', 3] ])->first();

		foreach ( $data as $name => $words ) {
			switch ( $name ) {
				case 'mahulas':
					$this->populate( $cat, $mahulas, $words );
					break;
				case 'medyo_mahulas':
					$this->populate( $cat, $medyo, $words );
					break;
				case 'mabudlay':
					$this->populate( $cat, $mabudlay, $words );
					break;
			}
		}
    }

    public function populate( $category, $level, $words )
    {
    	$teacherid = User::where('name','=','Teacher')->first()->id;

    	foreach ( $words as $name ) {
    		$fname = ( $name == 'liog' ) ? $name . '.jpeg' : $name . '.png';
    		$question = new Question();
    		$question->name = $name;
            $question->image = $fname;
            $question->sound = '';
            $question->category_id = $category->id;
            $question->level_id = $level->id;
            $question->teacher_id = $teacherid;
            $question->save();
    	}
    }
}
