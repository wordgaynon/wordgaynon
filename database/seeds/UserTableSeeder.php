<?php

use Illuminate\Database\Seeder;
use App\User;
use App\School;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$admin = new User();
        $admin->name = 'Administrator';
        $admin->first_name = 'Super';
    	$admin->last_name = 'Admin';
    	$admin->email = 'admin@admin.com';
    	$admin->password = bcrypt('secret');
    	$admin->save();

    	$admin->roles()->attach( Role::where('name', 'admin')->first() );

    	$teacher = new User();
    	$teacher->name = 'Teacher';
        $teacher->first_name = 'Proctor';
        $teacher->last_name = 'One';
    	$teacher->email = 'teacher@teacher.com';
        $teacher->password = bcrypt('secret');
    	$teacher->school_id = School::first()->id;
    	$teacher->save();
    	$teacher->roles()->attach( Role::where('name', 'teacher')->first() );
    }
}






