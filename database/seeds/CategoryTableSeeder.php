<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\User;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $uid = User::where('name','=','Teacher')->first()->id;
        $cats = array('lawas','butang','sapat','tanum');

        foreach ( $cats as $cat ) {
            $thecat = new Category();
            $thecat->name = $cat;
            $thecat->user_id = $uid;
            $thecat->save();
        }
    }
}
