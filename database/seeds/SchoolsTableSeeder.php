<?php

use Illuminate\Database\Seeder;
use App\School;

class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $school = new School();
		$school->name = 'Rizal Elementary Shcool';
		$school->address = 'Rizal St. Pototan, Iloilo City';
		$school->phone = '';
		$school->email = 'rizal@edu.ph';
        $school->save();
    }
}
