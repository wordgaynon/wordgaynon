<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function( Blueprint $table ) {
            $table->increments('id');
            $table->string('name', 191);
            $table->string('medals', 191)->default(0);
            $table->string('points', 191)->default(0);
            $table->text('datas')->nullable();
            $table->string('device_uuid', 191)->nullable();
            $table->integer('school_id')->nullable()->unsigned();
            $table->integer('teacher_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
