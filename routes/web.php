<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if ( Auth::check() ) {
		return view('welcome');	
	} else {
		return redirect('login');
	}
});

Auth::routes();

// admin routes
Route::get('home', 'HomeController@index')->name('home');
Route::get('students', 'HomeController@students')->name('students');
Route::get('certificate/{id}', 'HomeController@certificate')->name('certificate');

// settings
Route::get('settings', 'SettingsController@index')->name('settings');
Route::post('settings/profile', 'SettingsController@profile_update')->name('profile.update');
Route::post('settings/password', 'SettingsController@password_update')->name('password.update');

// Restful Controllers
Route::resources([
	'users' => 'UsersController',
	'roles' => 'RolesController',
	'category' => 'CategoryController',
	'questions' => 'QuestionsController',
	'schools' => 'SchoolsController',
]);

// Get levels
Route::get('levels/{category_id}', 'LevelsController@index')->name('levels');
Route::get('levels/{category_id}/create', 'LevelsController@create')->name('levels.create');
Route::post('levels/{category_id}', 'LevelsController@store')->name('levels.store');
Route::get('levels/{id}/edit', 'LevelsController@edit')->name('levels.edit');
Route::put('levels/{id}', 'LevelsController@update')->name('levels.update');
Route::delete('levels/{id}', 'LevelsController@destroy')->name('levels.destroy');

// API route
Route::get('api/admin', 'ApiController@get_admin');
Route::get('api/teachers', 'ApiController@get_teachers');
Route::get('api/newstudent', 'ApiController@newstudent');
Route::get('api/savestudent', 'ApiController@savestudent');
Route::get('api/students', 'ApiController@get_students');


